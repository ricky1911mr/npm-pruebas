const sumar= require('./sumar.js') //Cualquiera de las dos maneras 
const restar= require('./restar.js')
const multiplicar= require('./multiplicar.js')

module.exports = {
    sumar,
    restar,
    multiplicar
}